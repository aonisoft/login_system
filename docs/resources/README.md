# Usando plantuml

Si se usa visual code, se recomienda configurarlo de la siguiente manera:

Instalar la extencion: 

`Name: PlantUML`  
`Id: jebbs.plantuml`  
`Description: Rich PlantUML support for Visual Studio Code.`  
`Publisher: jebbs`  
`VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml`  

> Para ver su modo de uso, revisar su README.md

En setting.json que se encuentra en la carpeta .vscode(dentro de este proyecto)
colocar

```json
{
    "plantuml.diagramsRoot": "docs",
    "plantuml.exportOutDir": "docs/resources/",
    "plantuml.exportSubFolder": false
}
```
Esto le indica al plugin plantuml donde debe dejar las imagenes resultantes.

> Para generarlo apretar la combinacion de teclas `ctrl+shif+p` aparecera un
> display de buscador. Colocar `plantuml export` y seleccionar el que dice: 
> `plantUML: Export Current File Diagrams`

> En caso de que la carpeta `.vscode` no este, crearla manualmente y agregar el
> el archivo `.setting.json`  
> `Esta carpeta no debe ser seguida por git`. Por lo tanto, agregarla al 
> `.gitignore`
 
**Agregar mas informacion en caso de usar otro editor de codigo.**
