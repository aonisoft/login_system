# Modulos

Aqui se puede colocar los diagramas necesarios junto con la documentacion 
correspondiente.

Las imagenes generadas por plantuml estan el directorio resources de docs.

Ante cualquier duda sobre como se generan los diagramas de forma automatica,
revisar el [archivo](../../resources/README.md)