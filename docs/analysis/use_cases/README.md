# Diagrama inicial

![img](/docs/resources/analysis/use_cases/uml.svg)

# Ejemplos de uso en codigo

anUser = new ReadUserHandle( $id, $request );

aRuleDelete = new Rule( "user", [ "delete" ] );

anUser->canAccess( aRuleDelete );


# Ejemplos de roles

Administrador -> permiso: Crear usuario
                 no permiso: Eliminar usuario

Tarea: eliminar
Acceso: Vista usuario

Regla: eliminar usuario


# ¿Que es la clase Task?

Es la accion que se puede hacer. Esto esta relacionado con el contexto.


# ¿Que es el contexto?

El espacio donde el usuario entra. Puede ser una pagina especifica.

# Ejemplos de reglas con roles

Redactor

- [ ] Eliminar 
- [X] Crear
- [ ] Editar perfil 

Noticias

- [ ] Eliminar 
- [X] Crear
- [ ] Publicar
- [ ] Editar noticia 
- [ ] Cambiar estado


# Para que seria registro de actividad

Consta de un registro de actividades que se crea cuando un usuario dispara un 
evento( creacion de un articulo, eliminar un redactor, etc) y se genera un 
informe de lo que se hizo. 

Una actividad sería: 
- Descripcion del evento: iniciar sesion.
- Fecha y hora de realizacion: 08-09-2021 18:19.
- Estado: Fallo contraseña.
- Usuario: Pepe perez.